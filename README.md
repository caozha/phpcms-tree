# phpcms-tree 无限级别分类

#### 介绍
phpcms-tree，是一个从PHPCMS v9最新版中提取出来的无限级别分类的源码，可以整合到任何项目中。

## 重要说明

## 本项目已经迁移，请您到以下的新网址去下载和使用：


 **Gitee（国内仓库）：** https://gitee.com/dengzhenhua/phpcms-tree

 **GitHub（国外仓库）：** https://github.com/dengcao/phpcms-tree

